import { combine, Store } from "effector";

export function combinePendings(pendings: Store<boolean>[]): Store<boolean> {
  return combine(pendings, (arr) => arr.some(Boolean));
}
