import React, { useState } from "react";
import { useStore } from "effector-react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import InputAdornment from "@mui/material/InputAdornment";
import Button from "@mui/material/Button";
import {
  FormControl,
  IconButton,
  InputLabel,
  OutlinedInput,
} from "@mui/material";
import Visibility from "@mui/icons-material/Visibility";
import { VisibilityOff } from "@mui/icons-material";
import PersonIcon from "@mui/icons-material/Person";
import KeyIcon from "@mui/icons-material/Key";
import style from "./style.module.scss";
import { authService } from "../../services/auth-service/auth-service";
import { useNavigate } from "react-router-dom";

export const LoginPage = () => {
  const { login, password, loginFormIsValid, errorPassword, errorLogin } =
    useStore(authService.$values);
  const [showPassword, setShowPassword] = useState(false);

  const toggleShowPassword = () => setShowPassword(!showPassword);

  const loginValid = (login: string, password: string) => {
    authService.login();

    if (login === "login" && password === "login") {
      navigate("/dashboard");
    }
  };
  const form = document.getElementById("signup");
  form?.addEventListener("submit", (event) => {
    event.preventDefault();
  });
  const navigate = useNavigate();
  return (
    <div className={style.loginPage}>
      <form
        id="signup"
        onSubmit={() => loginValid(login, password)}
        className={style.loginBlock}
      >
        <h2>Вход в аккаунт</h2>
        <Box
          component="form"
          sx={{
            "& > :not(style)": {
              m: 3,
              width: "30ch",
              backgroundColor: "#ffffff",
            },
          }}
          noValidate
          autoComplete="off"
        >
          <TextField
            id="outlined-uncontrolled"
            label="Email"
            value={login}
            error={errorLogin}
            helperText={errorLogin ? "Неверный логин" : ""}
            onChange={(e) => authService.loginChanged(e.target.value)}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <PersonIcon />
                </InputAdornment>
              ),
            }}
          />
        </Box>
        <FormControl
          sx={{ m: 1, width: "30ch", backgroundColor: "#ffffff" }}
          variant="outlined"
        >
          <InputLabel htmlFor="outlined-adornment-password">Пароль</InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type={showPassword ? "text" : "password"}
            value={password}
            error={errorPassword}
            onChange={(e) => authService.passwordChanged(e.target.value)}
            startAdornment={
              <InputAdornment position="start">
                <KeyIcon />
              </InputAdornment>
            }
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={toggleShowPassword}
                  edge="end"
                >
                  {showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            label="Пароль"
          />
          {errorPassword && (
            <span className={style.errorPassword}>Неверный пароль</span>
          )}
        </FormControl>
        <Button
          disabled={!loginFormIsValid}
          sx={{ m: 2, color: "#ffffff", backgroundColor: " #6b6c6f" }}
          variant="contained"
          onClick={() => loginValid(login, password)}
        >
          Войти в систему
        </Button>
      </form>
    </div>
  );
};
