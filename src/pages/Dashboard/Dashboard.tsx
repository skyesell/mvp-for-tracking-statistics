import React, { useRef } from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  BarElement,
  Title,
  ArcElement,
  Tooltip,
  Legend,
  Filler,
  registerables,
  Ticks,
  Scale,
} from "chart.js";
import { Line, Bubble, Bar } from "react-chartjs-2";
import { Header } from "../../components/Header";
import s from "./style.module.scss";
import { dashboardService } from "../../services/dashboard-service/dashboard-service";
import { useGate, useStore } from "effector-react";

import {
  Tab,
  Box,
  Select,
  SelectChangeEvent,
  MenuItem,
  FormControl,
  ToggleButtonGroup,
  ToggleButton,
} from "@mui/material";
import { TabList, TabContext, TabPanel } from "@mui/lab";
import { Spinner } from "../../components/Spinner";
import { Complexity } from "../../api/dashboard/type";
import { retinaScale } from "chart.js/types/helpers";

ChartJS.register(
  ArcElement,
  CategoryScale,
  LinearScale,
  BarElement,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  Filler,
  ...registerables
);

const getDataForLineGraph = (
  labels: string[],
  label: string,
  dataArr: Number[],
  backgroundColor: string,
  borderColor: string
) => {
  return {
    labels: labels,
    lineTension: 0,
    datasets: [
      {
        label: label,
        data: dataArr,
        fill: true,
        tension: 0.5,
        backgroundColor: backgroundColor,
        borderColor: borderColor,
      },
    ],
  };
};

const getOptionsForLineGraph = {
  responsive: true,
  scales: {
    y: {
      ticks: {
        precision: 0,
      },
    },
  },
  plugins: {
    transitions: {},
    legend: {
      display: true,
      position: "bottom" as const,
      align: "start" as const,
      maxWidth: 310,
      labels: { boxWidth: 10, boxHeight: 10 },
    },
    title: {
      display: false,
    },
    scales: {
      y: {
        ticks: {
          stepSize: 1,
        },
      },
    },
  },
};

const optionsLineMob = {
  responsive: true,
  scales: {
    y: {
      ticks: {
        precision: 0,
      },
    },
  },
  plugins: {
    legend: {
      position: "bottom" as const,
      align: "start" as const,
      maxWidth: 310,
      labels: { boxWidth: 10, boxHeight: 10 },
    },
    title: {
      display: false,
    },
  },
};
type BubbleData = { x: string; y: number; r: number };

const getDataBubble = (arr2: number[]): BubbleData[] => {
  const arr: BubbleData[] = [];
  for (let i in arr2) {
    arr.push({
      x: i,
      y: arr2[i],
      r: 5 * arr2[i],
    });
  }
  return arr;
};

export const Dashboard = () => {
  const chartRef: React.MutableRefObject<any> = useRef(null);

  useGate(dashboardService.gate);

  const {
    xValues,
    averageAge,
    complexityInfo,
    totalNumber,
    happinessIndex,
    interval,
    racesInfo,
    genderInfo,
  } = useStore(dashboardService.$store);

  const isLoading = useStore(dashboardService.$isLoading);

  const [value, setValue] = React.useState("1");

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue);
  };

  const handleChangeSelect = (event: SelectChangeEvent) => {
    setValue(event.target.value);
  };

  const dataRacesLine = {
    labels: xValues,
    datasets: [
      {
        label: "Монголоидная ",
        data: racesInfo ? racesInfo.southeastAsian : [],
        tension: 0.5,
        fill: true,
        backgroundColor: "rgba(92, 137, 252, 0.5)",
        borderColor: "#5c89fc",
      },
      {
        label: "Арменоидная",
        data: racesInfo ? racesInfo.middleEastern : [],
        tension: 0.5,
        fill: true,
        backgroundColor: "rgb(110,222,119, 0.5)",
        borderColor: "rgb(110,222,119)",
      },
      {
        label: "Американоидная",
        data: racesInfo ? racesInfo.latinoHispanic : [],
        tension: 0.5,
        backgroundColor: "rgb(240,245,91)",
        borderColor: "rgb(240,245,91)",
      },
      {
        label: "Европеоидная",
        data: racesInfo ? racesInfo.white : [],
        tension: 0.5,
        backgroundColor: "rgb(219,82,69)",
        borderColor: "rgb(219,82,69)",
      },
      {
        label: "Эфиопская",
        data: racesInfo ? racesInfo.india : [],
        tension: 0.5,
        backgroundColor: "rgb(252,92,220)",
        borderColor: "rgb(252,92,220)",
      },
      {
        label: "Веддо-австралоидная",
        data: racesInfo ? racesInfo.eastAsian : [],
        tension: 0.5,
        backgroundColor: "rgb(35,212,197)",
        borderColor: "rgb(35,212,197)",
      },
      {
        label: "Негроидная",
        data: racesInfo ? racesInfo.black : [],
        tension: 0.5,
        backgroundColor: "rgb(125,35,212)",
        borderColor: "rgb(125,35,212)",
      },
    ],
  };
  const dataGenderLine = {
    labels: xValues,
    datasets: [
      {
        label: "Мужской",
        data: genderInfo ? genderInfo.Male : [],
        tension: 0.5,
        fill: true,
        backgroundColor: "rgba(92, 137, 252, 0.3)",
        borderColor: "#5c89fc",
      },
      {
        label: "Женский",
        data: genderInfo ? genderInfo.Female : [],
        tension: 0.5,
        backgroundColor: "rgba(252, 92, 101, 1)",
        borderColor: "#FC5C65",
      },
    ],
  };

  const dataComplexityLine = {
    labels: xValues,
    datasets: [
      {
        label: "Худой",
        data: complexityInfo ? complexityInfo.skinny : [],
        tension: 0.5,
        backgroundColor: "rgba(43, 203, 186, 0.12)",
        borderColor: "rgba(43, 203, 186, 1)",
      },
      {
        label: "Средний",
        data: complexityInfo ? complexityInfo.average : [],
        tension: 0.5,
        backgroundColor: "rgba(252, 92, 101, 1)",
        borderColor: "#FC5C65",
      },
      {
        label: "Упитанный",
        data: complexityInfo ? complexityInfo.fat : [],
        tension: 0.5,
        backgroundColor: "rgb(240,245,91)",
        borderColor: "#f0f55b",
      },
    ],
  };
  const dataEmotionLine = {
    labels: xValues,
    datasets: [
      {
        label: "Злой",
        data: happinessIndex ? happinessIndex.angry : [],
        tension: 0.5,
        barThickness: 20,
        backgroundColor: "rgba(252, 92, 101, 1)",
        borderColor: "#FC5C65",
      },
      {
        label: "Чувствующий отвращение",
        data: happinessIndex ? happinessIndex.disgust : [],
        tension: 0.5,
        barThickness: 20,
        backgroundColor: "#FD9644",
        borderColor: "#FD9644",
      },
      {
        label: "Испуганный",
        data: happinessIndex ? happinessIndex.fear : [],
        tension: 0.5,
        barThickness: 20,
        fill: true,
        backgroundColor: "rgba(43, 203, 186, 0.12)",
        borderColor: "#2BCBBA",
      },
      {
        label: "Счастливый",
        data: happinessIndex ? happinessIndex.happy : [],
        tension: 0.5,
        barThickness: 20,
        backgroundColor: "#45AAF2",
        borderColor: "#45AAF2",
      },
      {
        label: "Спокойный",
        data: happinessIndex ? happinessIndex.neutral : [],
        tension: 0.5,
        barThickness: 20,
        backgroundColor: "#4F7BFF",
        borderColor: "#4F7BFF",
      },
      {
        label: "Грустный",
        data: happinessIndex ? happinessIndex.sad : [],
        tension: 0.5,
        barThickness: 20,
        backgroundColor: "#A55EEA",
        borderColor: "#A55EEA",
      },
      {
        label: "Удивленный",
        data: happinessIndex ? happinessIndex.surprise : [],
        tension: 0.5,
        barThickness: 20,
        backgroundColor: "#26DE81",
        borderColor: "#26DE81",
      },
    ],
  };

  return (
    <div>
      <Header />
      <div className={s.dashboardWrapper}>
        <div className={s.select}>
          <FormControl sx={{ m: 1, minWidth: 300 }} size="small">
            <Select
              labelId="demo-select-small"
              id="demo-select-small"
              value={value}
              onChange={handleChangeSelect}
            >
              <MenuItem value="1">Количество</MenuItem>
              <MenuItem value="2">Раса</MenuItem>
              <MenuItem value="3">Пол</MenuItem>
              <MenuItem value="4">Возраст</MenuItem>
              <MenuItem value="5">Комплекция</MenuItem>
              <MenuItem value="6">Эмоции</MenuItem>
            </Select>
          </FormControl>
        </div>
        <Box
          className={s.dashboard}
          sx={{ width: "100%", typography: "body1" }}
        >
          <TabContext value={value}>
            <Box
              className={s.listItem}
              sx={{ borderBottom: 1, borderColor: "divider" }}
            >
              <TabList
                onChange={handleChange}
                aria-label="lab API tabs example"
              >
                <Tab label="Количество" value="1" />
                <Tab label="Раса" value="2" />
                <Tab label="Пол" value="3" />
                <Tab label="Возраст" value="4" />
                <Tab label="Комплекция" value="5" />
                <Tab label="Эмоции" value="6" />
              </TabList>
            </Box>

            <div className={s.dashboardChart}>
              <div className={s.toggleButton}>
                <ToggleButtonGroup
                  color="primary"
                  value={interval}
                  exclusive
                  size="small"
                  aria-label="Platform"
                >
                  <ToggleButton
                    onClick={() => dashboardService.changeButton("day")}
                    value="day"
                    disabled={isLoading}
                  >
                    В день
                  </ToggleButton>
                  <ToggleButton
                    onClick={() => dashboardService.changeButton("week")}
                    value="week"
                    disabled={isLoading}
                  >
                    В неделю
                  </ToggleButton>
                  <ToggleButton
                    onClick={() => dashboardService.changeButton("month")}
                    value="month"
                    disabled={isLoading}
                  >
                    В месяц
                  </ToggleButton>
                  <ToggleButton
                    onClick={() => dashboardService.changeButton("year")}
                    value="year"
                    disabled={isLoading}
                  >
                    В год
                  </ToggleButton>
                </ToggleButtonGroup>
              </div>

              <TabPanel value="1">
                <h2>Количество посетителей</h2>
                <div className={s.chart}>
                  {isLoading ? (
                    <Spinner />
                  ) : (
                    <>
                      <Line
                        ref={chartRef}
                        data={getDataForLineGraph(
                          xValues,
                          "Количество посетителей",
                          totalNumber,
                          "rgba(79, 123, 255, 0.12)",
                          "rgba(79, 123, 255, 1)"
                        )}
                        options={getOptionsForLineGraph}
                      />
                    </>
                  )}
                </div>
              </TabPanel>
              <TabPanel value="2">
                <div className={s.chart}>
                  <h2>Раса посетителей</h2>
                  {isLoading ? (
                    <Spinner />
                  ) : (
                    <>
                      <Line
                        ref={chartRef}
                        data={dataRacesLine}
                        options={optionsLineMob}
                      />
                    </>
                  )}
                </div>
              </TabPanel>
              <TabPanel value="3">
                <div className={s.chart1}>
                  <h2>Пол посетителей</h2>
                  <div className={s.chart}>
                    {isLoading ? (
                      <Spinner />
                    ) : (
                      <>
                        <Line
                          ref={chartRef}
                          data={dataGenderLine}
                          options={optionsLineMob}
                        />
                      </>
                    )}
                  </div>
                </div>
              </TabPanel>
              <TabPanel value="4">
                <div className={s.chart}>
                  <h2>Возраст посетителей</h2>
                  {isLoading ? (
                    <Spinner />
                  ) : (
                    <>
                      <Line
                        ref={chartRef}
                        data={getDataForLineGraph(
                          xValues,
                          "Возраст посетителей",
                          averageAge,
                          "rgba(79, 123, 255, 0.12)",
                          "rgba(79, 123, 255, 1)"
                        )}
                        options={getOptionsForLineGraph}
                      />
                    </>
                  )}
                </div>
              </TabPanel>
              <TabPanel value="5">
                <div className={s.chart}>
                  <h2>Комплекция посетителей</h2>
                  {isLoading ? (
                    <Spinner />
                  ) : (
                    <>
                      <Line
                        ref={chartRef}
                        data={dataComplexityLine}
                        options={getOptionsForLineGraph}
                      />
                    </>
                  )}
                </div>
              </TabPanel>
              <TabPanel value="6">
                <div className={s.chart}>
                  <h2>Эмоции посетителей</h2>
                  {isLoading ? (
                    <Spinner />
                  ) : (
                    <>
                      <>
                        <div className={s.chartDes}>
                          <Line
                            ref={chartRef}
                            data={dataEmotionLine}
                            options={optionsLineMob}
                          />
                        </div>
                        <div className={s.chartMob}>
                          <Line
                            height={"250px"}
                            width={"335px"}
                            data={dataEmotionLine}
                            options={optionsLineMob}
                          />
                        </div>{" "}
                      </>
                    </>
                  )}
                </div>
              </TabPanel>
            </div>
          </TabContext>
        </Box>
      </div>
    </div>
  );
};
