import { FormControl, TextField } from "@mui/material";
import React from "react";
import styles from "./styles.module.scss";
import cs from "classnames";
import Button from "@mui/material/Button";
import closeImg from "../img/Group 14.svg";
import Box from "@mui/material/Box";
import { settingsService } from "../../../services/settings-service/settings-service";
type PopupPropsType = {
  title: string;
  active: boolean;
  setActive: () => void;

  description?: string;
  url?: string;
  name?: string;
  callbackForSaveOrEdit: () => void;
};

export const Popup = ({
  title,
  active,
  setActive,
  description,
  url,
  callbackForSaveOrEdit,
  name,
}: PopupPropsType) => {
  return (
    <div
      className={cs(styles.popup, {
        [styles.active]: active,
      })}
      onClick={setActive}
    >
      <div
        className={cs(styles.popupContent, {
          [styles.active]: active,
        })}
        onClick={(e) => e.stopPropagation()}
      >
        <img src={closeImg} onClick={setActive} />
        <p>{title}</p>
        <Box sx={{ minWidth: 120 }}>
          <FormControl fullWidth>
            <div className={styles.formWrapper}>
              <TextField
                style={{ marginBottom: "40px" }}
                value={name}
                label={"Имя"}
                variant={"outlined"}
                placeholder="Имя"
                onChange={(e) => settingsService.nameChanged(e.target.value)}
              />
              <TextField
                style={{ marginBottom: "40px" }}
                label={"Описание"}
                value={description}
                variant={"outlined"}
                placeholder="Описание"
                onChange={(e) =>
                  settingsService.descriptionChanged(e.target.value)
                }
              />
              <TextField
                style={{ marginBottom: "40px" }}
                label={"URL камеры"}
                variant={"outlined"}
                value={url}
                onChange={(e) => {
                  settingsService.urlChanged(e.target.value);
                }}
              />
              <div className={styles.btnWrapper}>
                <Button
                  onClick={callbackForSaveOrEdit}
                  className={styles.btnSave}
                  variant="contained"
                  disabled={!(name && url)}
                >
                  Сохранить
                </Button>
                <Button
                  className={styles.btnClose}
                  variant="contained"
                  onClick={setActive}
                >
                  Отмена
                </Button>
              </div>
            </div>
          </FormControl>
        </Box>
      </div>
    </div>
  );
};
