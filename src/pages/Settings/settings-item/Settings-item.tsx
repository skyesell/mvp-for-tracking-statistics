import React from "react";
import styles from "./styles.module.scss";
import { settingsService } from "../../../services/settings-service/settings-service";
import { Button } from "@mui/material";
import { useStore } from "effector-react";

type SettingsItemPropsType = {
  title: string;
  url: string;
  editCallBack: (id: number) => void;
  deleteCallBack: (id: number) => void;
  id: number;
  cameraConnection: (url: string) => void;
};

export const SettingsItem = ({
  title,
  url,
  editCallBack,
  deleteCallBack,
  id,
  cameraConnection,
}: SettingsItemPropsType) => {
  const cameraConnectionPending = useStore(
    settingsService.$cameraConnectionPending
  );

  const { loading } = useStore(settingsService.$store);
  return (
    <div className={styles.settingsItemWrapper}>
      <p>{title}</p>
      <p className={styles.urlText}>{url}</p>
      <Button
        disabled={cameraConnectionPending || loading}
        onClick={() => cameraConnection(url)}
        variant="outlined"
      >
        Проверка соединения
      </Button>
      <div className={styles.btnWrapper}>
        <Button
          disabled={cameraConnectionPending || loading}
          className={styles.settingsBtnSave}
          onClick={() => editCallBack(id)}
        >
          Редактировать
        </Button>
        <span className={styles.trait} />
        <Button
          disabled={cameraConnectionPending || loading}
          className={styles.settingsBtnDelete}
          onClick={() => deleteCallBack(id)}
        >
          Удалить
        </Button>
      </div>
    </div>
  );
};
