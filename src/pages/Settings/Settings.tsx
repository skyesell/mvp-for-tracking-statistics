import React from "react";
import { Header } from "../../components/Header";
import plus from "./img/Group 36.png";
import styles from "./style.module.scss";
import { useGate, useStore } from "effector-react";
import { settingsService } from "../../services/settings-service/settings-service";
import { Popup } from "./Popup";
import { CustomButton } from "../../components/CustomButton";
import { SettingsItem } from "./settings-item";
import { Spinner } from "../../components/Spinner";

export const Settings = () => {
  const {
    nameCamera,
    description,
    url,
    allCamera,
    camera,
    popupIsOpenEdit,
    popupIsOpenAdd,
    loading,
  } = useStore(settingsService.$store);

  const CameraConnectionPending = useStore(
    settingsService.$cameraConnectionPending
  );
  const saveNewCamera = () => {
    settingsService.addNewCamera({
      name: nameCamera,
      url: url,
      description: description,
      id_camera: 0,
    });
    settingsService.tooglePopapAdd();
  };

  const patchCamera = (id: number) => {
    settingsService.patchCamera({
      name: nameCamera,
      url: url,
      description: description,
      id_camera: id,
    });
    settingsService.tooglePopapEdit();
  };

  const selectedPopup = (id: number) => {
    settingsService.selectCamera(id);
    settingsService.tooglePopapEdit();
  };

  const deleteCamera = (id: number) => {
    settingsService.deleteCamera(id);
  };

  const getCameraConnection = (url: string) => {
    settingsService.getCameraConnection(url);
  };

  useGate(settingsService.gate);
  const cameraConnectionPending = useStore(
    settingsService.$cameraConnectionPending
  );

  return (
    <div>
      <Header />
      <div className={styles.settingsWrapper}>
        <div className={styles.settingsHeaderWrapper}>
          <h1>Настройки</h1>
          <CustomButton
            callBack={settingsService.tooglePopapAdd}
            title={"Добавить камеру"}
            img={plus}
            right
          />
        </div>
        {cameraConnectionPending ||
          (loading && (
            <div className={styles.spinner}>
              <Spinner />
            </div>
          ))}
        <div className={styles.contentWrapper}>
          {allCamera.map((t) => (
            <>
              {CameraConnectionPending ?? <Spinner />}
              <SettingsItem
                key={t.id_camera}
                cameraConnection={getCameraConnection}
                title={t.name}
                url={t.url}
                deleteCallBack={deleteCamera}
                editCallBack={selectedPopup}
                id={t.id_camera}
              />
            </>
          ))}
        </div>
      </div>

      {popupIsOpenAdd && (
        <Popup
          active={popupIsOpenAdd}
          setActive={settingsService.tooglePopapAdd}
          title={"Добавить новое устройство"}
          description={description}
          callbackForSaveOrEdit={saveNewCamera}
          name={nameCamera}
          url={url}
        />
      )}
      {popupIsOpenEdit && (
        <Popup
          key={camera.id_camera}
          title={"Редактирование информации устройства"}
          active={popupIsOpenEdit}
          name={nameCamera}
          url={url}
          description={description ?? ""}
          setActive={settingsService.tooglePopapEdit}
          callbackForSaveOrEdit={() => patchCamera(camera.id_camera)}
        />
      )}
    </div>
  );
};
