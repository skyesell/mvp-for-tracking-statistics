import React from "react";
import s from "./style.module.scss";

export const Footer = () => {
  return (
    <div className={s.footerWrapper}>
      <div className={s.line} />
      <div className={s.underfoot}>
        <span>© «Технологии надежности» {new Date().getFullYear()}.&nbsp;</span>
        <span>Все права защищены</span>
      </div>
    </div>
  );
};
