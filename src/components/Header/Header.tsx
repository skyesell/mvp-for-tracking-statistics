import React from "react";
import { Link } from "react-router-dom";
import { useLocation } from "react-router-dom";
import cs from "classnames";
import SettingsIcon from "@mui/icons-material/Settings";
import LeaderboardIcon from "@mui/icons-material/Leaderboard";
import LogoutIcon from "@mui/icons-material/Logout";
import s from "./style.module.scss";

export const Header = () => {
  const location = useLocation();
  return (
    <div className={s.headerWrapper}>
      <div className={s.header}>
        <img
          className={s.logo}
          height={"52px"}
          width={"82px"}
          src={"/assets/logo.png"}
        />
        <div className={s.header}>
          <Link
            to="/dashboard"
            className={cs(s.headerLink, {
              [s.active]: location.pathname === "/dashboard",
            })}
          >
            <LeaderboardIcon />
            Dashboard
          </Link>
        </div>
        <Link
          to="/settings"
          className={cs(s.headerLink, {
            [s.active]: location.pathname === "/settings",
          })}
        >
          <SettingsIcon />
        </Link>
        <Link to="/" className={s.headerLink}>
          <LogoutIcon />
        </Link>
      </div>
    </div>
  );
};
