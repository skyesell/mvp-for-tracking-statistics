import styles from "../../pages/Settings/style.module.scss";
import React from "react";
import cs from "classnames";

type CustomButtonPropsType = {
  className?: string;
  callBack: () => void;
  title: string;
  img: any;
  left?: boolean;
  right?: boolean;
};

export const CustomButton = ({
  callBack,
  className,
  title,
  img,
  left,
  right,
}: CustomButtonPropsType) => {
  return (
    <div onClick={callBack} className={cs(styles.popupBtn, className)}>
      {left && <img src={img} />}
      <p>{title}</p>
      {right && <img src={img} />}
    </div>
  );
};
