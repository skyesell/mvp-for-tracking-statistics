import React from "react";
import s from "./style.module.scss";
import Box from "@mui/material/Box";
import { CircularProgress } from "@mui/material";

export const Spinner = () => {
  return (
    <div className={s.spinner}>
      <Box sx={{ display: "flex" }}>
        <CircularProgress />
      </Box>
    </div>
  );
};
