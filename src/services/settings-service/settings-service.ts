import { combine, createDomain, createEffect, forward, sample } from "effector";
import { createGate } from "effector-react";
import { settingsApi } from "../../api/settings";
import { Camera } from "../../api/settings/type";
import { combinePendings } from "../../helpers";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const gate = createGate();

const getAllCameraFx = createEffect({ handler: settingsApi.getAllCamera });
const saveCameraFx = createEffect({ handler: settingsApi.addCamera });
const deleteCameraFx = createEffect({ handler: settingsApi.deleteCamera });
const patchCameraFx = createEffect({ handler: settingsApi.patchCamera });
const getCameraFx = createEffect({ handler: settingsApi.getCamera });
const getCameraConnectionFx = createEffect({
  handler: settingsApi.getCameraConnection,
});
const settingsDomain = createDomain();

forward({ from: gate.open, to: getAllCameraFx });

const $allCameraStore = settingsDomain
  .createStore<Array<Camera>>([])
  .on(getAllCameraFx.doneData, (_, data) => data.reverse());

const $cameraStore = settingsDomain
  .createStore<Camera>({} as Camera)
  .on(getCameraFx.doneData, (_, data) => data);

const nameChanged = settingsDomain.createEvent<string>();
const descriptionChanged = settingsDomain.createEvent<string>();
const urlChanged = settingsDomain.createEvent<string>();
const selectCamera = settingsDomain.createEvent<number>();
const tooglePopapAdd = settingsDomain.createEvent();
const tooglePopapEdit = settingsDomain.createEvent();

const addNewCamera = settingsDomain.createEvent<Camera>();
const deleteCamera = settingsDomain.createEvent<number>();
const patchCamera = settingsDomain.createEvent<Camera>({});

const getCameraConnection = settingsDomain.createEvent<string>("");

const $nameCamera = settingsDomain.createStore("");
const $description = settingsDomain.createStore("");
const $url = settingsDomain.createStore("");

const $popupIsOpenAdd = settingsDomain.createStore<boolean>(false);
$popupIsOpenAdd.on(tooglePopapAdd, (value) => !value);

const $popupIsOpenEdit = settingsDomain.createStore<boolean>(false);
$popupIsOpenEdit.on(tooglePopapEdit, (value) => !value);

$popupIsOpenAdd.watch((i) => console.log(i));

$nameCamera.on(nameChanged, (_, name) => name).reset($popupIsOpenAdd);
$description
  .on(descriptionChanged, (_, description) => description)
  .reset($popupIsOpenAdd);
$url.on(urlChanged, (_, newUrl) => newUrl).reset($popupIsOpenAdd);

forward({ from: addNewCamera, to: saveCameraFx });

forward({ from: selectCamera, to: getCameraFx });
forward({ from: saveCameraFx.doneData, to: getAllCameraFx });

forward({ from: deleteCamera, to: deleteCameraFx });

forward({ from: getCameraConnection, to: getCameraConnectionFx });

forward({ from: deleteCameraFx.doneData, to: getAllCameraFx });

forward({ from: patchCamera, to: patchCameraFx });

forward({ from: patchCameraFx.doneData, to: getAllCameraFx });

const $loading = combinePendings([
  getAllCameraFx.pending,
  saveCameraFx.pending,
  deleteCameraFx.pending,
]);

sample({
  source: getCameraFx.doneData,
  fn: (camera) => camera.url,
  target: $url,
});

sample({
  source: getCameraFx.doneData,
  fn: (camera) => (camera.description ? camera.description : ""),
  target: $description,
});

sample({
  source: getCameraFx.doneData,
  fn: (camera) => camera.name,
  target: $nameCamera,
});
$url.watch((i) => console.log(i));

getCameraConnectionFx.done.watch(() =>
  toast.success("Соединение успешно установлено", {
    theme: "colored",
  })
);
getCameraConnectionFx.fail.watch(() =>
  toast.error("Соединение не установлено", { theme: "colored" })
);

deleteCameraFx.done.watch(() =>
  toast.success("Удалено успешно", {
    theme: "colored",
  })
);
deleteCameraFx.fail.watch(() =>
  toast.error("При удалении произошла ошибка", { theme: "colored" })
);

export const settingsService = {
  $store: combine({
    allCamera: $allCameraStore,
    nameCamera: $nameCamera,
    description: $description,
    url: $url,
    loading: $loading,
    camera: $cameraStore,
    popupIsOpenAdd: $popupIsOpenAdd,
    popupIsOpenEdit: $popupIsOpenEdit,
  }),
  gate,
  $cameraConnectionPending: getCameraConnectionFx.pending,
  nameChanged,
  selectCamera,
  tooglePopapAdd,
  tooglePopapEdit,
  descriptionChanged,
  urlChanged,
  addNewCamera,
  deleteCamera,
  patchCamera,
  getCameraConnection,
};
