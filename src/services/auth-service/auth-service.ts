import { combine, createDomain, sample } from "effector";

const authDomain = createDomain();

const loginChanged = authDomain.createEvent<string>();
const passwordChanged = authDomain.createEvent<string>();

const login = authDomain.createEvent();

const $login = authDomain.createStore("");
const $password = authDomain.createStore("");

const $errorLogin = authDomain.createStore(false).reset(loginChanged);
const $errorPassword = authDomain.createStore(false).reset(loginChanged);

$login.on(loginChanged, (_, login) => login).reset($errorLogin);
$password.on(passwordChanged, (_, password) => password).reset($errorPassword);
const $loginForm = combine({ $login, $password });

const $loginFormIsValid = authDomain.createStore(false);

sample({
  source: $login,
  clock: login,
  fn: (login) => login !== "login",
  target: $errorLogin,
});

sample({
  source: $password,
  clock: login,
  fn: (login) => login !== "login",
  target: $errorPassword,
});

$errorPassword.watch((i) => console.log(i));
$errorLogin.watch((i) => console.log(i));
sample({
  source: $loginForm,
  fn: (loginForm): boolean =>
    !!loginForm.$login.length && !!loginForm.$password.length,
  target: $loginFormIsValid,
});

export const authService = {
  $values: combine({
    login: $login,
    password: $password,
    loginFormIsValid: $loginFormIsValid,
    errorPassword: $errorPassword,
    errorLogin: $errorLogin,
  }),
  login,
  passwordChanged,
  loginChanged,
};
