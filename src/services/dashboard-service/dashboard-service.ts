import {
  createEffect,
  createDomain,
  sample,
  combine,
  createEvent,
} from "effector";
import { createGate } from "effector-react";
import { dashboardApi } from "../../api/dashboard";

import {
  Complexity,
  Dashboard,
  Gender,
  HappinessIndex,
  Races,
} from "../../api/dashboard/type";

type Interval = "day" | "week" | "month" | "year";
const gate = createGate();

const getGraphsFx = createEffect({ handler: dashboardApi.getDashboard });
const timeoutFx = createEffect(
  () => new Promise((res) => setTimeout(res, 5000))
);

timeoutFx.watch((i) => console.log(i));
const dashboardDomain = createDomain();

const $allDashboard = dashboardDomain
  .createStore<Dashboard | null>(null)
  .on(getGraphsFx.doneData, (_, data) => data);

const changeButton = createEvent<Interval>("");

const $interval = dashboardDomain
  .createStore<Interval>("day")
  .on(changeButton, (_, data) => data)
  .reset(gate.close);

const $xValues = dashboardDomain.createStore<string[]>([]);

const $totalNumber = dashboardDomain.createStore<number[]>([]);
const $averageAge = dashboardDomain.createStore<number[]>([]);
const $complexityInfo = dashboardDomain.createStore<Complexity | null>(null);
const $happinessIndex = dashboardDomain.createStore<HappinessIndex | null>(
  null
);
const $genderInfo = dashboardDomain.createStore<Gender | null>(null);
const $racesInfo = dashboardDomain.createStore<Races | null>(null);

sample({
  source: $interval,
  clock: gate.open,
  target: getGraphsFx,
});
sample({
  source: $interval,
  clock: changeButton,
  target: getGraphsFx,
});

// sample({
//   source: $interval,
//   clock: getGraphsFx.done,
//   target: timeoutFx,
// });
//
// sample({
//   source: getGraphsFx.done,
//   clock: timeoutFx.done,
//   fn: ({ params }) => params,
//   target: getGraphsFx,
// });

sample({
  source: $allDashboard,
  fn: (allDashboard) => (allDashboard ? allDashboard.graphData.x : []),
  target: $xValues,
});

sample({
  source: $allDashboard,
  fn: (allDashboard) =>
    allDashboard ? allDashboard.graphData.y.averageAge : [],
  target: $averageAge,
});

sample({
  source: $allDashboard,
  fn: (allDashboard) =>
    allDashboard ? allDashboard.graphData.y.complexityInfo : null,
  target: $complexityInfo,
});

sample({
  source: $allDashboard,
  fn: (allDashboard) =>
    allDashboard ? allDashboard.graphData.y.totalNumber : [],
  target: $totalNumber,
});

sample({
  source: $allDashboard,
  fn: (allDashboard) =>
    allDashboard ? allDashboard.graphData.y.happinessIndex : null,
  target: $happinessIndex,
});

sample({
  source: $allDashboard,
  fn: (allDashboard) =>
    allDashboard ? allDashboard.graphData.y.genderInfo : null,
  target: $genderInfo,
});

sample({
  source: $allDashboard,
  fn: (allDashboard) =>
    allDashboard ? allDashboard.graphData.y.racesInfo : null,
  target: $racesInfo,
});

export const dashboardService = {
  $store: combine({
    xValues: $xValues,
    averageAge: $averageAge,
    complexityInfo: $complexityInfo,
    totalNumber: $totalNumber,
    interval: $interval,
    happinessIndex: $happinessIndex,
    racesInfo: $racesInfo,
    genderInfo: $genderInfo,
  }),
  $isLoading: getGraphsFx.pending,
  gate,
  changeButton,
};
