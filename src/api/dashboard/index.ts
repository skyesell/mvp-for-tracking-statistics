import { http } from "../api";
import { Dashboard } from "./type";

export const dashboardApi = {
  getDashboard: async (interval: string): Promise<Dashboard> => {
    const res = await http.get(`/dashboard/test`, {
      params: { interval },
    });
    return res.data;
  },
};
