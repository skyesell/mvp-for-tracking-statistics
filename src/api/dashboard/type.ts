export type HappinessIndex = {
  angry: number[];
  disgust: number[];
  fear: number[];
  happy: number[];
  neutral: number[];
  sad: number[];
  surprise: number[];
};

export type Races = {
  eastAsian: number;
  southeastAsian: number;
  india: number;
  black: number;
  white: number;
  middleEastern: number;
  latinoHispanic: number;
};

export type Gender = {
  Male: number[];
  Female: number[];
};
export type Complexity = {
  skinny: number[];
  average: number[];
  fat: number[];
};
export type GraphData = {
  x: string[];
  y: {
    totalNumber: number[];
    averageAge: number[];
    complexityInfo: Complexity;
    happinessIndex: HappinessIndex;
    numOfReturnedPeople: number[];
    genderInfo: Gender;
    racesInfo: Races;
  };
};

export type Dashboard = {
  graphData: GraphData;
};
