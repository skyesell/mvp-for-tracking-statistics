import axios from "axios";

export const http = axios.create({
  baseURL: "https://smart-accounting-dev.reliab.tech/api/v1/",
});
