export interface Camera {
  url: string;
  name: string;
  id_camera: number;
  description: string | null;
}
