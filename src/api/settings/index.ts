import { http } from "../api";
import { Camera } from "./type";

export const settingsApi = {
  getAllCamera: async (): Promise<Array<Camera>> => {
    const res = await http.get(`/camera`);
    return res.data;
  },
  addCamera: async (params: Camera): Promise<void> => {
    const res = await http.post(`/camera/add`, {
      name: params.name,
      description: params.description,
      url: params.url,
    });
    return res.data;
  },
  patchCamera: async (params: Camera): Promise<void> => {
    const res = await http.patch(`/camera/patch`, {
      name: params.name,
      description: params.description,
      url: params.url,
      id_camera: params.id_camera,
    });
  },
  deleteCamera: async (id: number): Promise<void> => {
    const res = await http.delete(`/camera/delete/${id}`);
    return res.data;
  },
  getCamera: async (id: number): Promise<Camera> => {
    const res = await http.get(`/camera/get/${id}`);
    return res.data;
  },
  getCameraConnection: (url: string): Promise<void> =>
    http.get(`/camera/check`, {
      params: { url },
    }),
};
